#ifndef CURVEDATA_H
#define CURVEDATA_H
//------------------------------------------------------------------------------------
#include <qwt_series_data.h>
#include <QPointer>
#include <QVector>
#include <QPointF>
//#include <>
//#include <>
//#include <>

//#include ""
//#include ""
//#include ""
//------------------------------------------------------------------------------------
//! Обвертка над хранилищем данных, для отображения Qwt
//! Интерфейс Qwt
class CurveData: public QwtArraySeriesData <QPointF>
{
    public:
        CurveData();

        virtual QRectF boundingRect() const
        {
            if ( d_boundingRect.width() < 0.0 )
                 d_boundingRect = qwtBoundingRect( *this );

            return d_boundingRect;
        }

        inline void appendDataPoint( const QPointF &point )
        {
            d_samples += point;
        }

        void clear()
        {
            d_samples.clear();
            d_samples.squeeze();
            d_boundingRect = QRectF( 0.0, 0.0, -1.0, -1.0 );
        }
};

//------------------------------------------------------------------------------------
#endif // CURVEDATA_H
