#include "PlotView.h"

//------------------------------------------------------------------------------------
//!
PlotView::PlotView(QWidget *parent)
         :QwtPlot(parent),
          m_timeScaleDraw ( new TimeScaleDraw () ),
          m_valueScaleDraw ( new ValueScaleDraw() ),
          m_curve ( NULL ),
          m_grid ( NULL ),
          m_canvas ( NULL ),
          m_zoom ( NULL ),
          m_zoomer ( NULL ),
          m_magnifier ( NULL ),
          m_panner ( NULL ),
          m_curveTracker ( NULL ),
          m_picker ( NULL ),
          m_symbol ( new QwtSymbol() )
{
    setStyleSheet(//"font: 12px; "
                  //"font-weight: bold; "
                  "margin: 0px, 0px, 0px, 0px;"
                  "padding: 0px, 0px, 0px, 0px; "
                  //"border:  1px solid black; "
                  );

    //! Инициализация компонентов
    setup();

    setAutoReplot( true );

    //-------------------------------
    //! Установка шкалы оси X
    //setAxisTitle( QwtPlot::xBottom, "Время [hh:mm:ss]" );
    //setAxisScale( QwtPlot::xBottom, 0, m_countPoint );
    setAxisScaleDraw( QwtPlot::xBottom, m_timeScaleDraw );

//    setAxisLabelRotation( QwtPlot::xBottom, 0.0 );
//    setAxisLabelAlignment( QwtPlot::xBottom, Qt::AlignHCenter | Qt::AlignBottom );

    //! Включение автомасштабирования шкалы X
    //setAxisAutoScale(QwtPlot::xBottom, true);
    //setAxisAutoScale(QwtPlot::xBottom, false);

    //-------------------------------
    //! Установка шкалы оси Y
    //setAxisScale( QwtPlot::yLeft, 0.0, m_countPoint);
    setAxisScaleDraw( QwtPlot::yLeft, m_valueScaleDraw);

    //! Включение автомасштабирования шкалы Y
    //setAxisAutoScale(QwtPlot::yLeft | QwtPlot::yRight, true);
/*
    //! Подпись оси Y
    setAxisTitle( QwtPlot::yLeft, "Расход" );

    //! Установка шкалы оси Y
    setAxisScale( QwtPlot::yLeft, 0.0, 100.0 );
*/



    //plotLayout()->setAlignCanvasToScales( true );

    setMinimumHeight(30);
    setMinimumWidth(100);

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);



}
//------------------------------------------------------------------------------------
//!
PlotView::~PlotView()
{
    if (m_picker) delete m_picker;
    if (m_curveTracker) delete m_curveTracker;
    if (m_panner) delete m_panner;
    if (m_magnifier) delete m_magnifier;
    if (m_zoom) delete m_zoom;
//    if (m_zoomer) delete m_zoomer;

    if (m_grid) delete m_grid;
    if (m_canvas) delete m_canvas;
    if (m_curve) delete m_curve;

    //------------------------------------------
    //! Удаляется в QwtSymbol - QwtPlotCurve
    //! delete m_symbol;
    //------------------------------------------
    //! Удаляется в QwtScaleDraw - QwtScaleWidget - AxisData - QwtPlot
//    if(m_timeScaleDraw) delete m_timeScaleDraw;
//    if(m_valueScaleDraw) delete m_valueScaleDraw;
}
//------------------------------------------------------------------------------------
//! Инициализация компонентов
void PlotView::setup()
{
    //! Настройка диаграммы
    setupCurve();

    //! Настройка сетки
    setupGrid();

    //! Настройка подложки
    setupCanvas();

    //! Задание стартовой точки отсчета временной шкалы
    m_timeScaleDraw->setStartPoint( QDateTime::currentMSecsSinceEpoch() );

    //--------------------------------
    //! Масштабирование графика
    // #include <qwt_plot_zoomer.h>
    m_zoom = new QwtPlotZoomer(canvas());
    m_zoom->setRubberBandPen(QPen(Qt::red, 2, Qt::DotLine));
    m_zoom->setTrackerPen(QPen(Qt::red));
    //m_zoom->zoom(0);
    //--------------------------------
    //! Зумер графика с прокруткой
    //    m_zoomer = new Zoomer(m_plotView->canvas());
    //    m_zoomer->setRubberBandPen(QPen(Qt::red, 2, Qt::DotLine));
    //    m_zoomer->setTrackerPen(QPen(Qt::red));
    //    m_zoomer->zoom(0);
    //--------------------------------

    // Включить возможность приближения/удаления графика
    // #include <qwt_plot_magnifier.h>
    m_magnifier = new QwtPlotMagnifier(canvas());
    // клавиша, активирующая приближение/удаление
    m_magnifier->setMouseButton(Qt::MidButton);

    //--------------------------------
    // Включить возможность перемещения по графику
    // #include <qwt_plot_panner.h>
    m_panner = new QwtPlotPanner(canvas());
    // клавиша, активирующая перемещение
    m_panner->setMouseButton( Qt::RightButton, Qt::ControlModifier );

    //--------------------------------
    //! Линия слежения
    m_curveTracker = new CurveTracker(canvas());

    // for the demo we want the tracker to be active without
    // having to click on the canvas
    m_curveTracker->setStateMachine( new QwtPickerTrackerMachine() );
    m_curveTracker->setRubberBandPen( QPen( "MediumOrchid" ) );

    //--------------------------------
    /*
    //! Подсветка координат курсора
    QwtPlotPicker *m_picker = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft,
                                                //QwtPicker::PointSelection | QwtPicker::DragSelection,
                                                QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
                                                canvas());
    m_picker->setRubberBandPen(QColor(Qt::green));
    m_picker->setRubberBand(QwtPicker::CrossRubberBand);
    m_picker->setTrackerPen(QColor(Qt::white));
    */
    //-----------------------------------
    //! Подсветка ключевых точек графика
    m_symbol->setStyle(QwtSymbol::Ellipse);
    m_symbol->setPen(QColor(Qt::black));
    m_symbol->setSize(4);
    m_curve->setSymbol(m_symbol);
}
//------------------------------------------------------------------------------------
//!
void PlotView::replot()
{
//    axisWidget(QwtPlot::yLeft)->layoutScale(true);
//    axisWidget(QwtPlot::xBottom)->layoutScale(true);

    QwtPlot::replot();
}
//------------------------------------------------------------------------------------
//! Настройка диаграммы
void PlotView::setupCurve()
{
    //! Создадим кривые, потребуем, чтобы они были сглаженными (RenderAntialiased),
    //! укажем «перышки» (setPen()), и привяжем кривые к соответствующим областям рисования (attach()).
    m_curve = new QwtPlotCurve();

    //! Включить сглаживание
    m_curve->setRenderHint(QwtPlotItem::RenderAntialiased, true );
    m_curve->setPaintAttribute( QwtPlotCurve::ClipPolygons, false );

    //! Рисовать график линиями
    m_curve->setStyle( QwtPlotCurve::Lines );

    //! Рисовать зеленым
    m_curve->setPen( QPen( Qt::green ) );

    //! Задание хранилища данных кривой
    m_curve->setData( new CurveData() );

    //! Привязка кривой к графику
    m_curve->attach(this);
}
//------------------------------------------------------------------------------------
//!
void PlotView::setupCanvas()
{
    m_canvas = new QwtPlotCanvas (this);

    // The backing store is important, when working with widget
    // overlays ( f.e rubberbands for zooming ).
    // Here we don't have them and the internal
    // backing store of QWidget is good enough.

    //! Влияет на отрисовку сетки
//    m_canvas->setPaintAttribute( QwtPlotCanvas::BackingStore, false );
//    m_canvas->setBorderRadius( 10 );

    if ( QwtPainter::isX11GraphicsSystem() )
    {
#if QT_VERSION < 0x050000
        // Even if not liked by the Qt development, Qt::WA_PaintOutsidePaintEvent
        // works on X11. This has a nice effect on the performance.

        m_canvas->setAttribute( Qt::WA_PaintOutsidePaintEvent, true );
#endif

        // Disabling the backing store of Qt improves the performance
        // for the direct painter even more, but the canvas becomes
        // a native window of the window system, receiving paint events
        // for resize and expose operations. Those might be expensive
        // when there are many points and the backing store of
        // the canvas is disabled. So in this application
        // we better don't both backing stores.

        if ( m_canvas->testPaintAttribute( QwtPlotCanvas::BackingStore ) )
        {
            m_canvas->setAttribute( Qt::WA_PaintOnScreen, true );
            m_canvas->setAttribute( Qt::WA_NoSystemBackground, true );
        }
    }

    //! Настройка палитры подложки
    m_canvas->setPalette( setupPalette() );

    //! Графику подложку
    setCanvas( m_canvas );
}
//------------------------------------------------------------------------------------
//!
QPalette PlotView::setupPalette()
{
    QPalette pal = palette();

#if QT_VERSION >= 0x040400
    QLinearGradient gradient;
    gradient.setCoordinateMode( QGradient::StretchToDeviceMode );
    gradient.setColorAt( 0.0, QColor( 0, 49, 110 ) );
    gradient.setColorAt( 1.0, QColor( 0, 87, 174 ) );

    pal.setBrush( QPalette::Window, QBrush( gradient ) );
#else
    pal.setBrush( QPalette::Window, QBrush( color ) );
#endif

    // QPalette::WindowText is used for the curve color
    pal.setColor( QPalette::WindowText, Qt::green );

    return pal;
}
//------------------------------------------------------------------------------------
//!
void PlotView::setupGrid()
{
    m_grid = new QwtPlotGrid();
    m_grid->setPen( Qt::gray, 0.0, Qt::DotLine );
    m_grid->enableX( true );
    m_grid->enableXMin( true );
    m_grid->enableY( true );
    m_grid->enableYMin( false );
    m_grid->attach( this );
}
//------------------------------------------------------------------------------------
//!
