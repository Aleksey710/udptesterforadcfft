#ifndef VALUESCALEDRAW_H
#define VALUESCALEDRAW_H
//------------------------------------------------------------------------------------
#include <QDateTime>
#include <QObject>
#include <QString>
#include <qwt_scale_draw.h>
//#include <>
//#include <>
//#include <>
//#include <>

//#include ""
//#include ""
//#include ""

//------------------------------------------------------------------------------------
//!
class ValueScaleDraw: public QwtScaleDraw
{
    public:
        inline ValueScaleDraw( )
            { }

        inline virtual ~ValueScaleDraw()
            { }

        inline virtual QwtText label( double v ) const
            {
                if ( (v - (int) v) != 0 )
                {
                    return QString("%1").arg( QString::number(v,'f',2) );
                } else
                {
                    return QString("%1").arg( v );
                }
            }

    private:

};
//------------------------------------------------------------------------------------
#endif // VALUESCALEDRAW_H
