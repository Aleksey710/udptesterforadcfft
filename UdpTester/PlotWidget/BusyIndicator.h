#ifndef BUSYINDICATOR_H
#define BUSYINDICATOR_H
//------------------------------------------------------------------------------------
#include <QWidget>
#include <QPushButton>
#include <QLabel>
//#include <>
//#include <>
//#include <>

//#include ""
//#include ""
//#include ""

//------------------------------------------------------------------------------------
//!
class BusyIndicator : public QLabel
{
        Q_OBJECT

    public:
        enum State {
            Free = 0,
            Busy
        };

    public:
        explicit BusyIndicator(QWidget *parent = 0);
        virtual ~BusyIndicator();

    signals:

    public slots:
        void setState( State );

    private:
        State m_state;

};
//------------------------------------------------------------------------------------
#endif // BUSYINDICATOR_H
