#include "Zoomer.h"

//------------------------------------------------------------------------------------
//!
Zoomer::Zoomer(QWidget *canvas)
       :ScrollZoomer(canvas),
        m_rangeMax(5)
{
}
//------------------------------------------------------------------------------------
//!
void Zoomer::rescale()
{
    QwtScaleWidget *scaleWidget = plot()->axisWidget(yAxis());
    QwtScaleDraw *sd = scaleWidget->scaleDraw();

    double minExtent = 0;
    if ( zoomRectIndex() > 0 )
    {
        // When scrolling in vertical direction
        // the plot is jumping in horizontal direction
        // because of the different widths of the labels
        // So we better use a fixed extent.

        minExtent = sd->spacing() + sd->maxTickLength() + 1;
        minExtent += sd->labelSize(scaleWidget->font(), m_rangeMax).width();
    }

    sd->setMinimumExtent((quint16)minExtent);

    ScrollZoomer::rescale();
}
//------------------------------------------------------------------------------------
//!
