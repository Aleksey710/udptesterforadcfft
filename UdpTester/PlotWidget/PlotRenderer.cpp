#include "PlotRenderer.h"

//------------------------------------------------------------------------------------
//!
PlotRenderer::PlotRenderer(QObject *parent)
             :QwtPlotRenderer(parent)
{
}
//------------------------------------------------------------------------------------
//!
PlotRenderer::~PlotRenderer()
{

}
//------------------------------------------------------------------------------------
/*!
   \brief Execute a file dialog and render the plot to the selected file

   \param plot Plot widget
   \param documentName Default document name
   \param sizeMM Size for the document in millimeters.
   \param resolution Resolution in dots per Inch (dpi)

   \return True, when exporting was successful
   \sa renderDocument()
*/
bool PlotRenderer::exportTo( QwtPlot *plot,
                             const QString &documentName,
                             const QSizeF &sizeMM,
                             int resolution )
{
    if ( plot == NULL )
        return false;

    QString fileName = documentName;

    // What about translation

#ifndef QT_NO_FILEDIALOG
    const QList<QByteArray> imageFormats =
        QImageWriter::supportedImageFormats();

    QStringList filter;
#ifndef QT_NO_PRINTER
    filter += QString( "PDF " ) + tr( "Documents" ) + " (*.pdf)";
#endif
#ifndef QWT_NO_SVG
    filter += QString( "SVG " ) + tr( "Documents" ) + " (*.svg)";
#endif
#ifndef QT_NO_PRINTER
    filter += QString( "Postscript " ) + tr( "Documents" ) + " (*.ps)";
#endif

    if ( imageFormats.size() > 0 )
    {
        QString imageFilter( tr( "Images" ) );
        imageFilter += " (";
        for ( int i = 0; i < imageFormats.size(); i++ )
        {
            if ( i > 0 )
                imageFilter += " ";
            imageFilter += "*.";
            imageFilter += imageFormats[i];
        }
        imageFilter += ")";

        filter += imageFilter;
    }

    QFileDialog fileDialog;

    //! Список разрешенных для поиска адресов
    QList<QUrl> urls;
    urls << QUrl::fromLocalFile("/home");
    urls << QUrl::fromLocalFile("/home/user");
    urls << QUrl::fromLocalFile("/home/grey");
    //urls << QUrl::fromLocalFile("/");

    fileDialog.setSidebarUrls(urls);
    //fileDialog.setFileMode(QFileDialog::AnyFile);

    fileName = fileDialog.getSaveFileName( NULL,
                                           tr( "Зберегти файл" ),
                                           fileName,
                                           filter.join( ";;" ),
                                           NULL);

#endif
    if ( fileName.isEmpty() )
        return false;

    renderDocument( plot, fileName, sizeMM, resolution );

    return true;
}
