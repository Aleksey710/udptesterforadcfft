#ifndef PLOTRENDERER_H
#define PLOTRENDERER_H
//------------------------------------------------------------------------------------
#include <QImageWriter>
#include <QStringList>
#include <QFileDialog>
#include <qwt_plot_renderer.h>
#include <QList>
#include <QUrl>
//#include <>
//#include <>


//#include ""
//#include ""
//#include ""
//------------------------------------------------------------------------------------
//!
class PlotRenderer : public QwtPlotRenderer
{
        Q_OBJECT
    public:
        explicit PlotRenderer(QObject *parent = 0);
        virtual ~PlotRenderer();

        bool exportTo( QwtPlot *,
                       const QString &documentName,
                       const QSizeF &sizeMM = QSizeF( 297, 210 ),
                       int resolution = 100 );

    signals:

    public slots:

};
//------------------------------------------------------------------------------------
#endif // PLOTRENDERER_H
