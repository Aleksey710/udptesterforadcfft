#############################################################
TARGET      = UdpTester

#############################################################
#MAKE_RESULT = LIB
MAKE_RESULT = APP
#############################################################
# Приложение
contains( MAKE_RESULT, APP ) {
    message(--- MAKE_RESULT - APP!!! ---)

    TEMPLATE    = app
    DEPENDPATH += ../bin/
    DESTDIR     = ../bin/

    QT += gui
}
#############################################################
# Для библиотеки
contains( MAKE_RESULT, LIB ) {
    message(--- MAKE_RESULT - LIB!!! ---)

    TEMPLATE    = lib
    CONFIG      += staticlib
    DEPENDPATH  += ../lib/
    DESTDIR     = ../lib/

    #CONFIG   += dll

    #QT -= gui
}
#############################################################
#include(../.pri)
#############################################################

QT += core
QT += network
QT += svg

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += widgets \
          printsupport

    DEFINES += HAVE_QT5
}
#############################################################
OBJECTS_DIR = ../build/UdpTester
MOC_DIR     = ../build/UdpTester
UI_DIR      = ../build/UdpTester
RCC_DIR     = ../build/UdpTester

#############################################################
INCLUDEPATH += ../Qwt

INCLUDEPATH += ./
INCLUDEPATH += ./PlotWidget

#############################################################
HEADERS += \
    receiver.h \
    MainForm.h \
    PlotWidget/BusyIndicator.h \
    PlotWidget/CurveData.h \
    PlotWidget/CurveTracker.h \
    PlotWidget/IncrementalPlot.h \
    PlotWidget/PlotRenderer.h \
    PlotWidget/PlotView.h \
#    PlotWidget/PlotWidget.h \
    PlotWidget/ScrollBar.h \
    PlotWidget/ScrollZoomer.h \
    PlotWidget/TimeScaleDraw.h \
    PlotWidget/ValueScaleDraw.h \
    PlotWidget/Zoomer.h

#############################################################
SOURCES += main.cpp \
    receiver.cpp \
    MainForm.cpp \
    PlotWidget/BusyIndicator.cpp \
    PlotWidget/CurveData.cpp \
    PlotWidget/CurveTracker.cpp \
    PlotWidget/IncrementalPlot.cpp \
    PlotWidget/PlotRenderer.cpp \
    PlotWidget/PlotView.cpp \
#    PlotWidget/PlotWidget.cpp \
    PlotWidget/ScrollBar.cpp \
    PlotWidget/ScrollZoomer.cpp \
    PlotWidget/TimeScaleDraw.cpp \
    PlotWidget/ValueScaleDraw.cpp \
    PlotWidget/Zoomer.cpp


#############################################################
LIBS       += -L../lib -lQwt \

#############################################################

FORMS += \
    MainForm.ui

