#ifndef MAINFORM_H
#define MAINFORM_H
//------------------------------------------------------------------------------------
#include <QWidget>
#include <QHostAddress>
#include <QUdpSocket>
#include <QTimer>
//#include <>
//#include <>
//#include <>
//#include <>
//#include <>

//#include ""
//#include ""
//#include ""


#include "PlotView.h"
//------------------------------------------------------------------------------------
namespace Ui {
class MainForm;
}
//------------------------------------------------------------------------------------
//!
class MainForm : public QWidget
{
        Q_OBJECT

    private:
        static int COUNT;

        enum TypeDatagram {
            ADC = 0,
            FFT = 1
        };

    public:
        explicit MainForm(QWidget *parent = 0);
        virtual ~MainForm();

    public slots:
        void start();
        void stop();

        void sendData();

        void receiveProcessPendingDatagrams();
        void adcProcessPendingDatagrams();
        void fftProcessPendingDatagrams();

    private:
        void queryWasExecuted(const QByteArray &data, TypeDatagram type);

    private:
        Ui::MainForm *ui;
        QTimer *m_timer;

        QUdpSocket *m_receiveUdpSocket;
        QUdpSocket *m_adcUdpSocket;
        QUdpSocket *m_fftUdpSocket;

        QHostAddress m_address;
        quint16 m_receivePort;
        quint16 m_adcPort;
        quint16 m_fftPort;

        PlotView *m_adcPlotView;
        PlotView *m_fftPlotView;

        CurveData *m_adcCurveData;
        CurveData *m_fftCurveData;
};
//------------------------------------------------------------------------------------
#endif // MAINFORM_H
