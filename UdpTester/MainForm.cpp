#include "MainForm.h"
#include "ui_MainForm.h"
//------------------------------------------------------------------------------------
int MainForm::COUNT = 0;
//------------------------------------------------------------------------------------
//!
MainForm::MainForm(QWidget *parent)
         :QWidget(parent),
          ui(new Ui::MainForm),
          m_timer ( new QTimer(this) ),
          m_receiveUdpSocket ( new QUdpSocket(this) ),
          m_adcUdpSocket ( new QUdpSocket(this) ),
          m_fftUdpSocket ( new QUdpSocket(this) ),
          m_address( QHostAddress("192.168.1.4") ),
          m_receivePort ( 10000 ),
          m_adcPort ( 11000 ),
          m_fftPort ( 12000 ),
          m_adcPlotView ( new PlotView() ),
          m_fftPlotView ( new PlotView() )
{
    ui->setupUi(this);

    m_timer->setInterval(1000);

    connect(m_timer, SIGNAL(timeout()),
            this, SLOT( sendData()));

    connect(m_receiveUdpSocket, SIGNAL(readyRead()),
            this, SLOT(receiveProcessPendingDatagrams()));
    connect(m_adcUdpSocket, SIGNAL(readyRead()),
            this, SLOT(adcProcessPendingDatagrams()));
    connect(m_fftUdpSocket, SIGNAL(readyRead()),
            this, SLOT(fftProcessPendingDatagrams()));

    m_receiveUdpSocket->bind(m_receivePort, QUdpSocket::ShareAddress);
    m_adcUdpSocket->bind(m_adcPort, QUdpSocket::ShareAddress);
    m_fftUdpSocket->bind(m_fftPort, QUdpSocket::ShareAddress);

    //udpSocket->connectToHost("192.168.1.11", 10000, QIODevice::ReadWrite);

    ui->plotLayout->addWidget(m_adcPlotView);
    ui->plotLayout->addWidget(m_fftPlotView);

    //! Получить указатель на хранилище данных графика
    m_adcCurveData = static_cast<CurveData *>( m_adcPlotView->data()->data() );
    m_fftCurveData = static_cast<CurveData *>( m_fftPlotView->data()->data() );
}
//------------------------------------------------------------------------------------
//!
MainForm::~MainForm()
{
    delete ui;
}
//------------------------------------------------------------------------------------
//!
void MainForm::start()
{
    qDebug() <<  tr("START");

    m_timer->start();
}
//------------------------------------------------------------------------------------
//!
void MainForm::stop()
{
    qDebug() <<  tr("STOP");
    m_timer->stop();
}
//------------------------------------------------------------------------------------
//!
void MainForm::sendData()
{
    //qDebug() << "MainForm::sendData()";

    const char * data = QString("test%1").arg(COUNT++).toLatin1().data();

    qDebug() << "MainForm::sendData() -" << QString(data);

//    m_receiveUdpSocket->writeDatagram(data,
//                                      sizeof(data),
//                                      m_address,
//                                      //m_receivePort);
//                                      10000);

    m_adcUdpSocket->writeDatagram(data,
                                  sizeof(data),
                                  m_address,
                                  //m_receivePort);
                                  11000);

//    m_fftUdpSocket->writeDatagram(data,
//                                  sizeof(data),
//                                  m_address,
//                                  //m_receivePort);
//                                  12000);

}
//------------------------------------------------------------------------------------
//!
void MainForm::receiveProcessPendingDatagrams()
{
    qDebug() << "MainForm::receiveProcessPendingDatagrams()"
             << QString("PendingDatagramSize: %1")
                .arg(m_receiveUdpSocket->pendingDatagramSize());

    while (m_receiveUdpSocket->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(m_receiveUdpSocket->pendingDatagramSize());

        QHostAddress sender;
        quint16 senderPort;

        m_receiveUdpSocket->readDatagram(datagram.data(), datagram.size(),
                                         &sender, &senderPort);

        //processTheDatagram(datagram);
    }
}
//------------------------------------------------------------------------------------
//!
void MainForm::adcProcessPendingDatagrams()
{
    qDebug() << "MainForm::adcProcessPendingDatagrams() ";

    while (m_adcUdpSocket->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(m_adcUdpSocket->pendingDatagramSize());
        m_adcUdpSocket->readDatagram(datagram.data(), datagram.size());

//        qDebug() << QDateTime::currentDateTime().toString("HH:mm:ss:zzz")
//                 << QString("Received adc datagram: (%1) - %2")
//                    .arg(QString(datagram.toHex().data()))
//                    .arg(datagram.size());

        qDebug() << QDateTime::currentDateTime().toString("HH:mm:ss:zzz")
                 << QString("Received adc datagram: (%1)").arg(datagram.size());

        queryWasExecuted(datagram, ADC);
    }
}
//------------------------------------------------------------------------------------
//!
void MainForm::fftProcessPendingDatagrams()
{
    qDebug() << "MainForm::fftProcessPendingDatagrams() ";

    while (m_fftUdpSocket->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(m_fftUdpSocket->pendingDatagramSize());
        m_fftUdpSocket->readDatagram(datagram.data(), datagram.size());

//        qDebug() << QDateTime::currentDateTime().toString("HH:mm:ss:zzz")
//                 << QString("Received fft datagram: (%1) - %2")
//                    .arg(QString(datagram.toHex().data()))
//                    .arg(datagram.size());
        qDebug() << QDateTime::currentDateTime().toString("HH:mm:ss:zzz")
                 << QString("Received fft datagram: (%1)").arg(datagram.size());

        queryWasExecuted(datagram, FFT);
    }
}
//------------------------------------------------------------------------------------
//!
void MainForm::queryWasExecuted(const QByteArray &data, TypeDatagram type)
{
//    qDebug() << "MainForm::queryWasExecuted "
//             << data.size()
//             << ( (type == 0) ? "ADC" : "FFT" );

    if(data.size() == 0) return;

    //! Очистить график от предыдущих данных
    switch (type)
    {
        case ADC:
        {
            m_adcCurveData->clear();

        //    double minScaleY = 0;
        //    double maxScaleY = 0;

        //    double maxScaleX = 0;

            double X = 0;
            double Y = 0;

            double record = 0;

            for(int nPoint = 0; nPoint < data.size(); nPoint++)
            {
                //! Получить запись
                record = (unsigned char)data.at(nPoint);

                //record = record*100;
                //-----------------------------------------
                //! Данные по колонке 0 - Время в мс
                X = nPoint;

                //-----------------------------------------
                //! Вычислить максимальное значение по Х
                //if (X > maxScaleX) maxScaleX = X;

                //-----------------------------------------
                //! Данные по колонке 1 - измеренные значения
                Y = record;

        //        //! Минимальная амплитуда шкалы Y
        //        if (minScaleY > Y) minScaleY = Y ;

        //        //! Максимальная амплитуда шкалы Y
        //        if (maxScaleY < Y) maxScaleY = Y ;

                //! Добавить точку на график
                m_adcCurveData->appendDataPoint(QPointF(X, Y));
            }

            //! Задание стартовой точки отсчета временной шкалы
            //m_adcPlotView->timeScaleDraw()->setStartPoint(0);

            //--------------------------------------------------
            //! Странный способ обновления шкалы х
            m_adcPlotView->setAxisScale(QwtPlot::xBottom, 0, 1500);

            //--------------------------------------------------
            //! Чтоб график полностью помещался на холст
            m_adcPlotView->setAxisAutoScale(QwtPlot::yLeft, true);
            m_adcPlotView->setAxisAutoScale(QwtPlot::xBottom, true);

            m_adcPlotView->zoom()->setZoomBase( QRectF(0, 0, 1500, 260) );

            //--------------------------------------------------
            //! Задание зоны нормальности
            //setNormalRange(m_curentLowAlarm, m_curentHighAlarm);

            m_adcPlotView->replot();
        }
            break;
        case FFT:
        {
            m_fftCurveData->clear();

        //    double minScaleY = 0;
        //    double maxScaleY = 0;

        //    double maxScaleX = 0;

            double X = 0;
            double Y = 0;

            double record = 0;

            for(int nPoint = 0; nPoint < data.size(); nPoint++)
            {
                //! Получить запись
                record = (unsigned char)data.at(nPoint);

                //record = record*100;
                //-----------------------------------------
                //! Данные по колонке 0 - Время в мс
                X = nPoint;

                //-----------------------------------------
                //! Вычислить максимальное значение по Х
                //if (X > maxScaleX) maxScaleX = X;

                //-----------------------------------------
                //! Данные по колонке 1 - измеренные значения
                Y = record;

        //        //! Минимальная амплитуда шкалы Y
        //        if (minScaleY > Y) minScaleY = Y ;

        //        //! Максимальная амплитуда шкалы Y
        //        if (maxScaleY < Y) maxScaleY = Y ;

                //! Добавить точку на график
                m_fftCurveData->appendDataPoint(QPointF(X, Y));
            }

            //! Задание стартовой точки отсчета временной шкалы
            //m_adcPlotView->timeScaleDraw()->setStartPoint(0);

            //--------------------------------------------------
            //! Странный способ обновления шкалы х
            m_fftPlotView->setAxisScale(QwtPlot::xBottom, 0, 1500);

            //--------------------------------------------------
            //! Чтоб график полностью помещался на холст
            m_fftPlotView->setAxisAutoScale(QwtPlot::yLeft, true);
            m_fftPlotView->setAxisAutoScale(QwtPlot::xBottom, true);

            m_fftPlotView->zoom()->setZoomBase( QRectF(0, 0, 1500, 260) );

            //--------------------------------------------------
            //! Задание зоны нормальности
            //setNormalRange(m_curentLowAlarm, m_curentHighAlarm);

            m_fftPlotView->replot();
        }
            break;
        default:
            break;
    }
}
