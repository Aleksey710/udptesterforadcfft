/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/



#include "receiver.h"
int Receiver::COUNT = 0;

Receiver::Receiver(QWidget *parent)
         : QWidget(parent)
{
    statusLabel = new QLabel(tr("Listening for broadcasted messages"));
    statusLabel->setWordWrap(true);

    quitButton = new QPushButton(tr("&Quit"));

//! [0]
    m_adcUdpSocket = new QUdpSocket(this);
    m_adcUdpSocket->bind(10000, QUdpSocket::ShareAddress);
//! [0]    
    qDebug() <<  tr("START");

    QTimer::singleShot(1000, this, SLOT(sendData()));

//! [1]
    connect(m_adcUdpSocket, SIGNAL(readyRead()),
            this, SLOT(processPendingDatagrams()));
//! [1]
    connect(quitButton, SIGNAL(clicked()), this, SLOT(close()));

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addStretch(1);
    buttonLayout->addWidget(quitButton);
    buttonLayout->addStretch(1);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(statusLabel);
    mainLayout->addLayout(buttonLayout);
    setLayout(mainLayout);

    setWindowTitle(tr("Broadcast Receiver"));
}

void Receiver::processPendingDatagrams()
{
    //qDebug() <<  tr("PendingDatagramSize: %1").arg(udpSocket->pendingDatagramSize());

    while (m_adcUdpSocket->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(m_adcUdpSocket->pendingDatagramSize());
        m_adcUdpSocket->readDatagram(datagram.data(), datagram.size());
//        statusLabel->setText(tr("Received datagram: \"%1\"")
//                             .arg(datagram.data()));

        qDebug() <<  tr("Received datagram: \"%1\"").arg(datagram.data());
    }

}

void Receiver::sendData()
{
    qDebug() << "Receiver::sendData()";

    //udpSocket->connectToHost("192.168.1.11", 10000, QIODevice::ReadWrite);

    QHostAddress address("192.168.1.4");
    quint16 port = 11000;

    const char * data = QString("test%1").arg(COUNT++).toLatin1().data();

    qDebug() << "Receiver::sendData() -" << QString(data);

    m_adcUdpSocket->writeDatagram(data,
                                  sizeof(data),
                                  address, port);

    QTimer::singleShot(1000, this, SLOT(sendData()));
}










