#############################################################
TDM_CONFIG -= USE_GPROF
#############################################################
unix{
    TDM_CONFIG += USE_CCACHE
}
#############################################################
contains( TDM_CONFIG, USE_CCACHE ) {
    message(--- Use CCACHE enabled!!! ---)

#    QMAKE_CC    = ccache gcc -std=c++11
#    QMAKE_CXX   = ccache g++ -std=c++11

#    QMAKE_CC    = distcc ccache gcc
#    QMAKE_CXX   = distcc ccache g++

    QMAKE_CC    = ccache gcc
    QMAKE_CXX   = ccache g++

#    QMAKE_CC    = gcc
#    QMAKE_CXX   = g++
}
#############################################################
contains( TDM_CONFIG, USE_GPROF ) {
    message(--- Use GPROF enabled!!! ---)

    release {
        QMAKE_CFLAGS   += -pg
        QMAKE_CXXFLAGS += -pg
        QMAKE_LFLAGS   += -pg
    }

    debug {
        QMAKE_CFLAGS_DEBUG      += -pg
        QMAKE_CXXFLAGS_DEBUG    += -pg
        QMAKE_LFLAGS_DEBUG      += -pg
    }
}
#############################################################
#lrelease [options] project-file
#lrelease [options] ts-files [-qm qm-file]
#TRANSLATIONS = \
#    ../../bin/translations/tdm_ru.ts \
#    ../../bin/translations/tdm_ua.ts \
#    ../../bin/translations/tdm_en.ts



