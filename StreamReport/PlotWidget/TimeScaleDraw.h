#ifndef TIMESCALEDRAW_H
#define TIMESCALEDRAW_H
//------------------------------------------------------------------------------------
#include <QDateTime>
#include <qwt_scale_draw.h>
//#include <>
//#include <>

//#include ""
//#include ""
//#include ""

//------------------------------------------------------------------------------------
//!
class TimeScaleDraw: public QwtScaleDraw
{
    public:
        inline TimeScaleDraw( ) :
            m_startPoint ( 0 )
            { }

        inline void setStartPoint(const qint64 &value )
            {
                QDateTime dateTime;
                dateTime.setMSecsSinceEpoch(value);
                dateTime.toString("dd MMM yyyy\n"
                                  "hh:mm:ss");

                m_startPoint = value;
            }

        virtual QwtText label( double v ) const
            {
                QDateTime dateTime;

                qint64 labelPoint = m_startPoint + v;

                dateTime.setMSecsSinceEpoch(labelPoint);

//                qDebug() << "TimeScaleDraw::label"
//                         << v << labelPoint - m_startPoint
//                         << dateTime.toString("dd MMM yyyy hh:mm:ss:zzz");

                return dateTime.toString("dd MMM yyyy\n"
                                         "hh:mm:ss");
            }

    private:
        qint64 m_startPoint;

};
//------------------------------------------------------------------------------------
#endif // TIMESCALEDRAW_H
