#ifndef PLOTWIDGET_H
#define PLOTWIDGET_H
//------------------------------------------------------------------------------------
#include <QWidget>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <qwt_painter.h>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_canvas.h>
//#include <qwt_plot_directpainter.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_renderer.h>
#include <qwt_plot_zoneitem.h>
//#include <qwt_system_clock.h>
#include <limits>
#include <qglobal.h>
#include <QApplication>
//#include <>
//#include <>

//#include ""
//#include ""
//#include ""
#include "ManagerConfigurations.h"
#include "DatabaseConnectionThread.h"
#include "CurveData.h"
#include "PlotView.h"
#include "PlotRenderer.h"
#include "BusyIndicator.h"
#include "DatabaseConnection.h"
//------------------------------------------------------------------------------------
//!
class PlotWidget : public QWidget
{
        Q_OBJECT
    private:
        //! Для различия между собой
        static int CountPlotWidget;

    public:
        explicit PlotWidget(QWidget *parent = 0);

        virtual ~PlotWidget();

    signals:
        void newQuery(const QString &strQuery, const QString &strComent);

        void curentFromDateTimeChanged(const qint64 &curentFromDateTime );

        //! Хак для обеспечения работы в разных потоках
        void forwardQueryWasExecuted(const QVector<QSqlRecord> &);

    public slots:
        //! Задать начальные данные для поиска информации для графика
        void setQueryData(const QString &tableName,
                          const QDateTime &dateTime,
                          const int rangeTime,
                          const QString &yLeftTitle,
                          const double &lowAlarm,
                          const double &hightAlarm);

        void previosFrame();

        void nextFrame();

    private slots:
        //! Хак для обеспечения работы в разных потоках
        void proxyQueryWasExecuted(QVector<QSqlRecord> listRecord);
        void queryWasExecuted(const QVector<QSqlRecord> &listRecord);

        void exportDocument();

    private:
        //! Задание зоны нормальности
        void setNormalRange(double low, double hight);

    private:
        //! Слой отображения графика
        QHBoxLayout                 *m_layout;

        DatabaseConnectionThread    *m_databaseConnectionThread;

        //! Отображаемый график
        PlotView                    *m_plotView;

        //! Указатель на хранилище данных графика
        CurveData                   *m_curveData;

        //! Выделеная зона
        QwtPlotZoneItem             *m_zoneLow;
        QwtPlotZoneItem             *m_zoneHight;

        //---------------------------------------------------------
        //! Виджет кнопки печати и индикатора занятости
        QWidget                     *m_complexWidget;

        //! Слой виджета кнопки печати и индикатора занятости
        QVBoxLayout                 *m_complexLayout;

        //! Кнопка печати графика
        QPushButton                 *m_printButton;

        //! Инициализация индикатора занятости базы
        BusyIndicator               *m_databaseBusy;
        //---------------------------------------------------------
        //! Имя опрашиваемой таблицы (из последнего запроса)
        QString                     m_tableName;

        //! Текущий уровень нижней границы
        double                      m_curentLowAlarm;

        //! Текущий уровень верхней границы
        double                      m_curentHighAlarm;

        qint64                      m_curentFromDateTime;

        qint64                      m_curentRangeTime;
};
//------------------------------------------------------------------------------------
#endif // PLOTWIDGET_H
