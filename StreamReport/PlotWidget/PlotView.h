#ifndef PLOTVIEW_H
#define PLOTVIEW_H
//------------------------------------------------------------------------------------
#include <QWidget>
#include <QMouseEvent>
#include "qwt_global.h"
#include "qwt_painter.h"
#include <qwt_system_clock.h>
#include <qwt_symbol.h>
#include <qwt_picker_machine.h>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_magnifier.h>
#include <qwt_plot_panner.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_directpainter.h>
//#include <>
//#include <>
//#include <>

//#include ""
//#include ""
//#include ""
#include "TimeScaleDraw.h"
#include "ValueScaleDraw.h"
#include "CurveData.h"

#include "ScrollZoomer.h"
#include "Zoomer.h"
#include "CurveTracker.h"
//------------------------------------------------------------------------------------
//!
class PlotView : public QwtPlot
{
        Q_OBJECT
    public:
        explicit PlotView(QWidget *parent = 0);

        virtual ~PlotView();

        inline QwtPlotCurve* data()
            { return m_curve; }

        inline TimeScaleDraw* timeScaleDraw()
            { return m_timeScaleDraw; }

        inline QwtPlotZoomer* zoom()
            { return m_zoom; }

        inline Zoomer* zoomer()
            { return m_zoomer; }

    public slots:
        virtual void replot();

    private:
        //! Инициализация компонентов
        void setup();

        //! Настройка диаграммы
        void setupCurve();

        //! Настройка подложки
        void setupCanvas();

        //! Настройка палитры подложки
        QPalette setupPalette();

        //! Настройка сетки
        void setupGrid();


    private:
        //! Временная шкала
        TimeScaleDraw           *m_timeScaleDraw;

        //! Шкала измеряемого параметра
        ValueScaleDraw          *m_valueScaleDraw;

        //! Диаграмма
        QwtPlotCurve            *m_curve;

        //! Сетка
        QwtPlotGrid             *m_grid;

        //! Подложка
        QwtPlotCanvas           *m_canvas;

        //! Масштабирование графика
        QwtPlotZoomer               *m_zoom;

        //! Зумер отображаемого графика
        Zoomer                      *m_zoomer;

        // Включить возможность приближения/удаления графика
        QwtPlotMagnifier            *m_magnifier;

        // Перемещения по графику
        QwtPlotPanner               *m_panner;

        //! Линия слежения
        CurveTracker                *m_curveTracker;

        //! Подсветка координат курсора
        QwtPlotPicker               *m_picker;

        //! Подсветка ключевых точек графика
        QwtSymbol               *m_symbol;


};
//------------------------------------------------------------------------------------
#endif // PLOTVIEW_H
