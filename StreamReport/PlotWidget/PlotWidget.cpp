#include "PlotWidget.h"
//------------------------------------------------------------------------------------
//!
int PlotWidget::CountPlotWidget = 0;
//------------------------------------------------------------------------------------
//!
PlotWidget::PlotWidget(QWidget *parent)
           :QWidget(parent),
            m_layout ( new QHBoxLayout() ),
            m_databaseConnectionThread ( NULL ),
            m_plotView ( new PlotView(this) ),
            m_zoneLow ( NULL ),
            m_zoneHight ( NULL ),
            m_complexWidget ( new QWidget(this) ),
            m_complexLayout ( new QVBoxLayout() ),
            m_printButton ( new QPushButton("", this) ),
            m_databaseBusy ( new BusyIndicator(this) ),
            m_curentLowAlarm ( 0 ),
            m_curentHighAlarm ( 0 ),
            m_curentFromDateTime ( QDateTime::currentMSecsSinceEpoch() ),
            m_curentRangeTime ( 20 )
{
    //! Уникальный номер (для различия подключения к базе)
    CountPlotWidget++;

    setStyleSheet(//"font: 12px; "
                  //"font-weight: bold; "
                  "margin: 0px, 0px, 0px, 0px;"
                  "padding: 0px, 0px, 0px, 0px; "
                  //"border:  1px solid black; "
                  );

    setLayout(m_layout);

    //-------------------------------------------
    //! Инициализировать подключение к базе и модель результата запроса
    //initResultModel();

//    m_databaseConnectionThread = new DatabaseConnectionThread(QString("PlotWidget%1").arg(CountPlotWidget),
//                                                              this);

//    //! Установить флаг всегда переподключаться при запросе
//    m_databaseConnectionThread->setFlagAlwaysReconnecting( true );

//    connect(this, SIGNAL(newQuery(const QString &, const QString &)),
//            m_databaseConnectionThread, SLOT (executeQuery(const QString &, const QString &)));

    //! Оповещать об изменении текущих настроек подключения
    qRegisterMetaType < QVector<QSqlRecord> > ("QVector<QSqlRecord>");
//    connect(m_databaseConnectionThread, SIGNAL(queryExecuted(const QVector<QSqlRecord> &)),
//            this, SLOT (queryWasExecuted(const QVector<QSqlRecord> &)));

    connect(this, SIGNAL(forwardQueryWasExecuted(const QVector<QSqlRecord> &)),
            this, SLOT (queryWasExecuted(const QVector<QSqlRecord> &)));

    //-------------------------------------------
    //! Получить указатель на хранилище данных графика
    m_curveData = static_cast<CurveData *>( m_plotView->data()->data() );

    m_layout->addWidget(m_plotView);
    //--------------------------------
    //! Зона нормальности
    m_zoneHight = new QwtPlotZoneItem();
    m_zoneHight->setPen( Qt::darkGray );
    m_zoneHight->setBrush( QColor( "#834358" ) );
    //m_zone->setBrush( QColor(150,150,0,100) );
    m_zoneHight->setOrientation( Qt::Horizontal );
    m_zoneHight->setInterval( 0.0, 0.0 );
    m_zoneHight->attach( m_plotView );

    m_zoneLow = new QwtPlotZoneItem();
    m_zoneLow->setPen( Qt::darkGray );
    m_zoneLow->setBrush( QColor( "#834358" ) );
    //m_zone->setBrush( QColor(150,150,0,100) );
    m_zoneLow->setOrientation( Qt::Horizontal );
    m_zoneLow->setInterval( 0.0, 0.0 );
    m_zoneLow->attach( m_plotView );

    //--------------------------------
    //! Инициализация индикатора занятости базы
    m_complexLayout->addWidget(m_databaseBusy);

    //--------------------------------
    //! Инициализация иконки кнопки печати
    QIcon printButtonIcon = QIcon(QPixmap(":/img/img/printer.png",
                                          0,
                                          Qt::AutoColor ));

    //! Инициализация кнопки печати
    m_printButton->setIcon(printButtonIcon);
    m_printButton->setFixedHeight(50);
    m_printButton->setFixedWidth(50);

    m_printButton->setIconSize(m_printButton->size() * 0.7);

    m_printButton->setStyleSheet(//"font: 12px; "
                                 //"font-weight: bold; "
                                 "margin: 0px, 0px, 0px, 0px;"
                                 "padding: 0px, 0px, 0px, 0px; "
                                 //"border:  1px solid black; "
                                 );

    connect(m_printButton, SIGNAL(clicked()),
            this, SLOT(exportDocument()));

    m_complexLayout->addWidget(m_printButton);

    //--------------------------------
    m_complexWidget->setLayout(m_complexLayout);

    //! Добавит индикатор занятости базы и кнопку печати
    m_layout->addWidget(m_complexWidget);
}
//------------------------------------------------------------------------------------
//!
PlotWidget::~PlotWidget()
{
    CountPlotWidget--;

    delete m_zoneHight;
    delete m_zoneLow;
    delete m_plotView;
    delete m_printButton;
    delete m_layout;
}
//------------------------------------------------------------------------------------
//! Задать начальные данные для поиска информации для графика
void PlotWidget::setQueryData(const QString &tableName,
                              const QDateTime &dateTime,
                              const int rangeTime,
                              const QString &yLeftTitle,
                              const double &lowAlarm,
                              const double &highAlarm)
{
    m_tableName = tableName;

    //! Подпись оси Y
    m_plotView->setAxisTitle( QwtPlot::yLeft, yLeftTitle);

    m_curentLowAlarm = lowAlarm;
    m_curentHighAlarm = highAlarm;

    m_curentFromDateTime = dateTime.toMSecsSinceEpoch();

    m_curentRangeTime = rangeTime;

    //! Защита от дурака
    //if (m_curentRangeTime > 1000) m_curentRangeTime = 1000;

    m_curentRangeTime = rangeTime * 1000;

    //-----------------------------------------
    QString strQuery = QString("SELECT * FROM %1 "
                          "WHERE idDataTime BETWEEN '%2' AND '%3' "
                          //"WHERE idDataTime > '%2' "
                          "ORDER BY 'res.idDataTime' LIMIT 0 , %3 ;"
                          ).arg(m_tableName)
                           .arg(m_curentFromDateTime)
                           .arg(m_curentFromDateTime + m_curentRangeTime);

    //qDebug() << "PlotWidget::setQueryData()" << strQuery ;

    m_databaseBusy->setState(BusyIndicator::Busy);

    bool alwaysReconect = true;

    QString coment = QString("PlotWidget setQueryData for (%1)").arg(m_tableName);

    Delegate *delegat = new Delegate();
    delegat->connect( this, &PlotWidget::proxyQueryWasExecuted );

    DatabaseManagerUnit::Instance().execute(strQuery,
                                            alwaysReconect,
                                            delegat,
                                            coment);
}
//------------------------------------------------------------------------------------
//!
void PlotWidget::proxyQueryWasExecuted(QVector<QSqlRecord> listRecord)
{
    emit forwardQueryWasExecuted (listRecord);
}
//------------------------------------------------------------------------------------
//!
void PlotWidget::queryWasExecuted(const QVector<QSqlRecord> &listRecord)
{
    //qDebug() << "PlotWidget::queryExecuted Получено записей" << listRecord.size();

    m_databaseBusy->setState(BusyIndicator::Free);

    if(listRecord.size() == 0) return;

    //! Очистить график от предыдущих данных
    m_curveData->clear();

    double minScaleY = 0;
    double maxScaleY = 0;

    double maxScaleX = 0;

    double previousValueX = 0;

    double X = 0;
    double Y = 0;

    qint64 timeFromBaseMSecs = 0;

    QSqlRecord record;

    for(int nPoint = 0; nPoint < listRecord.size(); ++nPoint)
    {
        //! Получить запись
        record = listRecord.at(nPoint);

        //-----------------------------------------
        //! Данные по колонке 0 - Время в мс
        timeFromBaseMSecs = record.value(0).toLongLong();

        X = timeFromBaseMSecs - m_curentFromDateTime;

        //-----------------------------------------
        //! Если попадается 2 точки подряд с одинаковым временем -
        //! вторую не отображать
        if (previousValueX == X) continue ;

        //! Сохранить значение времени
        previousValueX = X;
        //-----------------------------------------
        //! Вычислить максимальное значение по Х
        if (X > maxScaleX) maxScaleX = X;

        //-----------------------------------------
        //! Данные по колонке 1 - измеренные значения
        Y = record.value(1).toDouble();

        //! Минимальная амплитуда шкалы Y
        if (minScaleY > Y) minScaleY = Y ;

        //! Максимальная амплитуда шкалы Y
        if (maxScaleY < Y) maxScaleY = Y ;

        //! Добавить точку на график
        m_curveData->appendDataPoint(QPointF(X, Y));
    }

    //! Задание стартовой точки отсчета временной шкалы
    m_plotView->timeScaleDraw()->setStartPoint(m_curentFromDateTime);

    //--------------------------------------------------
    //! Странный способ обновления шкалы х
    m_plotView->setAxisScale(QwtPlot::xBottom, 0, 800);

    //--------------------------------------------------
    //! Чтоб график полностью помещался на холст
    m_plotView->setAxisAutoScale(QwtPlot::yLeft, true);
    m_plotView->setAxisAutoScale(QwtPlot::xBottom, true);

    m_plotView->zoom()->setZoomBase( QRectF(0, 0, maxScaleX, maxScaleY) );

    //--------------------------------------------------
    //! Задание зоны нормальности
    setNormalRange(m_curentLowAlarm, m_curentHighAlarm);

    m_plotView->replot();
}
//------------------------------------------------------------------------------------
//!
void PlotWidget::previosFrame()
{
    m_databaseBusy->setState(BusyIndicator::Busy);

    //qint64 timeSize = 10 * 1000;
    qint64 timeSize = m_curentRangeTime * 0.7;

    m_curentFromDateTime = m_curentFromDateTime - timeSize;

    emit curentFromDateTimeChanged( m_curentFromDateTime );

    QString strQuery = QString("SELECT * FROM %1 "
                          "WHERE idDataTime BETWEEN '%2' AND '%3' "
                          //"WHERE idDataTime > '%2' "
                          "ORDER BY 'res.idDataTime' LIMIT 0 , %3 ;"
                          ).arg(m_tableName)
                           .arg(m_curentFromDateTime)
                           .arg(m_curentFromDateTime + m_curentRangeTime);

    //emit newQuery(strQuery, QString("PlotWidget::previosFrame()(%1)").arg(m_tableName));

    bool alwaysReconect = true;

    QString coment = QString("PlotWidget::previosFrame()(%1)").arg(m_tableName);

    Delegate *delegat = new Delegate();
    delegat->connect( this, &PlotWidget::proxyQueryWasExecuted );

    DatabaseManagerUnit::Instance().execute(strQuery,
                                            alwaysReconect,
                                            delegat,
                                            coment);
}
//------------------------------------------------------------------------------------
//!
void PlotWidget::nextFrame()
{
    m_databaseBusy->setState(BusyIndicator::Busy);

    qint64 timeSize = m_curentRangeTime * 0.7;

    m_curentFromDateTime = m_curentFromDateTime + timeSize;

    emit curentFromDateTimeChanged( m_curentFromDateTime );

    QString strQuery = QString("SELECT * FROM %1 "
                          "WHERE idDataTime BETWEEN '%2' AND '%3' "
                          //"WHERE idDataTime > '%2' "
                          "ORDER BY 'res.idDataTime' LIMIT 0 , %3 ;"
                          ).arg(m_tableName)
                           .arg(m_curentFromDateTime)
                           .arg(m_curentFromDateTime + m_curentRangeTime);

    //emit newQuery(strQuery, QString("PlotWidget::nextFrame() (%1)").arg(m_tableName));

    bool alwaysReconect = true;

    QString coment = QString("PlotWidget::nextFrame() (%1)").arg(m_tableName);

    Delegate *delegat = new Delegate();
    delegat->connect( this, &PlotWidget::proxyQueryWasExecuted );

    DatabaseManagerUnit::Instance().execute(strQuery,
                                            alwaysReconect,
                                            delegat,
                                            coment);
}
//------------------------------------------------------------------------------------
//! Задание зоны нормальности
void PlotWidget::setNormalRange(double low, double hight)
{
//    if(m_zoneHight && m_zoneLow)
//    {
        //max = pow(2,sizeof(double) * 8.0 - 1) - 1
//        dMinValue = std::numeric_limits<double>::min(),
        //dMaxValue = std::numeric_limits<double>::max();

        //m_zoneHight = new QwtPlotZoneItem();
        //m_zoneHight->setPen( Qt::darkGray );
        //m_zoneHight->setBrush( QColor( "#834358" ) );
        //m_zone->setBrush( QColor(150,150,0,100) );
        //m_zoneHight->setOrientation( Qt::Horizontal );

        //! double - 10 значащих цифр +/- 999999.0 для равномерности
        m_zoneHight->setInterval( hight, 99999.0 );
        //m_zoneHight->setInterval( hight, std::numeric_limits<double>::max() );

        //m_zoneHight->attach( m_plotView );

        //m_zoneLow = new QwtPlotZoneItem();
        //m_zoneLow->setPen( Qt::darkGray );
        //m_zoneLow->setBrush( QColor( "#834358" ) );
        //m_zone->setBrush( QColor(150,150,0,100) );
        //m_zoneLow->setOrientation( Qt::Horizontal );

        //! double - 10 значащих цифр +/- 99999.0 для равномерности
        m_zoneLow->setInterval(-99999.0, low );
        //m_zoneLow->setInterval(std::numeric_limits<double>::min(), low );


        //m_zoneLow->attach( m_plotView );
//    }
}
//------------------------------------------------------------------------------------
//!
void PlotWidget::exportDocument()
{
    //! Переделаный рендер
    PlotRenderer  renderer;

    renderer.exportTo( m_plotView, QString("Plot_%1_%2.pdf")
                       .arg(m_plotView->axisTitle(QwtPlot::yLeft).text())
                       .arg(QDateTime::currentDateTime().toString("yyyy.MM.dd hh-mm-ss")));
}
