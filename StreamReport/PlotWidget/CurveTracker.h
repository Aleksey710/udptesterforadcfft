#ifndef _CURVE_TRACKER_
#define _CURVE_TRACKER_H_
//------------------------------------------------------------------------------------
#include <qwt_plot_picker.h>
#include <QDateTime>



//------------------------------------------------------------------------------------
class QwtPlotCurve;
//------------------------------------------------------------------------------------
//!
class CurveTracker: public QwtPlotPicker
{
    public:
        CurveTracker( QWidget * );

    protected:
        virtual QwtText trackerTextF( const QPointF & ) const;
        virtual QRect trackerRect( const QFont & ) const;

    private:
        //! Отображаемый текст возле линии слежения
        QString curveInfoAt( const QwtPlotCurve *, const QPointF & ) const;
        QLineF curveLineAt( const QwtPlotCurve *, qreal x ) const;
};
//------------------------------------------------------------------------------------
//!
#endif
