#ifndef ZOOMER_H
#define ZOOMER_H
//------------------------------------------------------------------------------------
#include <QMouseEvent>

#include <qwt_plot_canvas.h>
#include <qwt_scale_widget.h>


#include "ScrollZoomer.h"
//------------------------------------------------------------------------------------
//!
class Zoomer: public ScrollZoomer
{
        Q_OBJECT
    public:

        Zoomer(QWidget *canvas);

        inline void setRangeMax(double rangeMax)
            { m_rangeMax = rangeMax; }

        virtual void rescale();

    private:
        double m_rangeMax;

};
//------------------------------------------------------------------------------------
#endif // ZOOMER_H
