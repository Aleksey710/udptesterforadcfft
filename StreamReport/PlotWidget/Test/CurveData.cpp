#include "CurveData.h"
//------------------------------------------------------------------------------------
//!
/*
CurveData::CurveData()
          :m_data( NULL )
{
    m_data = new BufferData();
}
//------------------------------------------------------------------------------------
//!
CurveData::~CurveData()
{

}
//------------------------------------------------------------------------------------
//!
void CurveData::setSourceData(BufferData* data)
{
    if (m_data) delete m_data;

    m_data = data;
}
//------------------------------------------------------------------------------------
//!
const BufferData * CurveData::values() const
{
    return m_data;
}
//------------------------------------------------------------------------------------
//!
BufferData * CurveData::values()
{
    return m_data;
}
//------------------------------------------------------------------------------------
//!
QPointF CurveData::sample( size_t i ) const
{
    return m_data->value( i );
}
//------------------------------------------------------------------------------------
//!
size_t CurveData::size() const
{
    return m_data->size();
}
//------------------------------------------------------------------------------------
//!
QRectF CurveData::boundingRect() const
{
    return m_data->boundingRect();
}
//------------------------------------------------------------------------------------
//!
*/
CurveData::CurveData()
{

}
