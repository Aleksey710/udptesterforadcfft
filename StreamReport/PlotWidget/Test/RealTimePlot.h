#ifndef REALTIMEPLOT_H
#define REALTIMEPLOT_H
//------------------------------------------------------------------------------------
#include <QWidget>
#include <Qwt/qwt_plot.h>
#include <Qwt/qwt_plot_curve.h>
#include <Qwt/qwt_plot_grid.h>
#include <Qwt/qwt_plot_canvas.h>
#include <Qwt/qwt_painter.h>
#include <Qwt/qwt_plot_directpainter.h>
#include <Qwt/qwt_system_clock.h>
#include <Qwt/qwt_symbol.h>
#include <QTimerEvent>
#include <QShowEvent>
//#include <>
//#include <>
//#include <>
//#include <>

//#include ""
//#include ""
//#include ""
#include "CurveData.h"
//------------------------------------------------------------------------------------
//!
class RealTimePlot : public QwtPlot
{
        Q_OBJECT
    public:
        RealTimePlot(QWidget *parent = 0);
        ~RealTimePlot();

    public slots:
        void timerHandle();

    private:

        QwtPlotCurve *curve;

        QwtPlotDirectPainter *painter;

        void setData();

        void appendGraphPoint(QPointF point);

        int counter;

        double x[1000];

        double y[1000];
        /*
        Объект curve понадобится непосредственно для рисования кривой,
        painter – для дорисовки точек в реальном времени,
        counter – тут будем хранить номер точки в массиве данных, которая должна быть добавлена на график следующей.
        */
};
//------------------------------------------------------------------------------------
#endif // REALTIMEPLOT_H
