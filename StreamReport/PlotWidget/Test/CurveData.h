#ifndef CURVEDATA_H
#define CURVEDATA_H
//------------------------------------------------------------------------------------
#include <Qwt/qwt_series_data.h>
#include <QPointer>
//#include <>
//#include <>
//#include <>

//#include ""
//#include ""
//#include ""
#include "BufferData.h"
//------------------------------------------------------------------------------------
//! Обвертка над хранилищем данных, для отображения Qwt
//! Интерфейс Qwt
/*
class CurveData: public QwtSeriesData<QPointF>
{
    public:
        //! Конструктор и деструктор
        //! от предка - QwtSeriesData<QPointF>

        CurveData();
        ~CurveData();

        void setSourceData(BufferData* data);

    public:
        const BufferData* values() const;

        BufferData* values();

        virtual QPointF sample( size_t i ) const;

        virtual size_t size() const;

        virtual QRectF boundingRect() const;

    private:
        BufferData        *m_data;
};
*/
class CurveData: public QwtArraySeriesData <QPointF>
{
    public:
        CurveData();

        virtual QRectF boundingRect() const
        {
            if ( d_boundingRect.width() < 0.0 )
                 d_boundingRect = qwtBoundingRect( *this );

            return d_boundingRect;
        }

        inline void appendDataPoint( const QPointF &point )
        {
            d_samples += point;
        }

        void clear()
        {
            d_samples.clear();
            d_samples.squeeze();
            d_boundingRect = QRectF( 0.0, 0.0, -1.0, -1.0 );
        }
};

//------------------------------------------------------------------------------------
#endif // CURVEDATA_H
