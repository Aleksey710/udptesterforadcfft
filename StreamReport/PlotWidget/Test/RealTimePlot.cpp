#include "RealTimePlot.h"
//------------------------------------------------------------------------------------
//!
RealTimePlot::RealTimePlot(QWidget *parent)
             :QwtPlot(parent)
{
    counter = 0;
    painter = new QwtPlotDirectPainter(this);
    this->setAxisScale(QwtPlot::xBottom, -1, 1);
    this->setAxisScale(QwtPlot::yLeft, 0, 1);
    curve = new QwtPlotCurve("y(x)");
    curve->setStyle(QwtPlotCurve::NoCurve);
    curve->setData( new CurveData() );
    curve->setSymbol( new QwtSymbol(QwtSymbol::Ellipse,
                                    Qt::NoBrush,
                                    QPen(Qt::red),
                                    QSize(1, 1) ) );
    curve->attach(this);
    setAutoReplot(false);
    setData();
}
//------------------------------------------------------------------------------------
//!
RealTimePlot::~RealTimePlot()
{
    //delete zoomer;
    delete painter;
    delete curve;
}
//------------------------------------------------------------------------------------
//!
void RealTimePlot::timerHandle()
{
    QPointF newPoint = QPointF(x[counter],
                               y[counter]);
    counter++;

    appendGraphPoint(newPoint);
}
//------------------------------------------------------------------------------------
//! Добавить точку на отображение
void RealTimePlot::appendGraphPoint(QPointF point)
{
    CurveData *data = static_cast<CurveData *>( curve->data() );
    data->appendDataPoint(point);
    painter->drawSeries(curve, 0, data->size() - 1);
}
//------------------------------------------------------------------------------------
//!
void RealTimePlot::setData()
{
    const int n = 1000;
    double h = 2.0 / n;
    for(int i = 0; i < n; i++)
    {
        x[i] = -1 + i * h;
        y[i] = qAbs(x[i]);
    }
}
//------------------------------------------------------------------------------------
//!


