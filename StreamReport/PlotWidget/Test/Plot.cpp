#include "Plot.h"
//------------------------------------------------------------------------------------
//!
Plot::Plot(QWidget *parent)
     :QwtPlot(parent),
      m_curve( NULL ),
      m_grid( NULL ),
      m_canvas( NULL ),
      m_interval( 10.0 ),
      m_timerId( -1 ),
      m_directPainter( NULL ),
      m_paintedPoints( 0 )
{
    qDebug() << "Plot::Plot";

    m_directPainter = new QwtPlotDirectPainter();

    //setAutoReplot( false );
    setAutoReplot( true );

    const int margin = 0;
    setContentsMargins( margin, margin, margin, margin );

    //! Подпись снизу оси X
    setAxisTitle( QwtPlot::xBottom, "Время [hh:mm:ss]" );

    //! Установка шкалы оси X
    //setAxisScale( QwtPlot::xBottom, 0.0, 3600.0 );
    setAxisScaleDraw( QwtPlot::xBottom,
                      new TimeScaleDraw( QTime::currentTime () ) );
/*
    //! Подпись оси Y
    setAxisTitle( QwtPlot::yLeft, "Расход" );

    //! Установка шкалы оси Y
    setAxisScale( QwtPlot::yLeft, 0.0, 100.0 );
*/

    //! Настройка диаграммы
    setupCurve();

    //! Настройка сетки
    setupGrid();

    //! Настройка подложки
    setupCanvas();

    //! Нарисовать тестовую диаграмму
    //setTestData();
    setData();


    //! Запуск часов Qwt
    m_clock.start();

    if ( m_timerId >= 0 )
    {
        killTimer( m_timerId );
    }

    m_timerId = startTimer( 1 );


    //! Таймер для частоты обновления
    m_updateDataTimer = new QTimer(this);
    connect(m_updateDataTimer, SIGNAL(timeout()), this, SLOT(timerHandle()));
    m_updateDataTimer->start(1);
}
//------------------------------------------------------------------------------------
//!
Plot::~Plot()
{
    if (m_directPainter) delete m_directPainter;
    if (m_curve) delete m_curve;
    if (m_grid) delete m_grid;
    if (m_canvas) delete m_canvas;
}
//------------------------------------------------------------------------------------
//!
void Plot::setData()
{
    int N = 500;
    double amplitude = 80;

    for (int x = 0; x <= N; ++x)
    {
        double v = amplitude * qFastSin( x * M_PI/180 );

        m_x[x] = x;
        m_y[x] = v;

        //QPointF s( x, v );

        //bufferData->append(s);
    }

    /*
    for(int i = 0; i < n; i++)
    {
        m_x[i] = i;
        m_y[i] = amplitude * qFastSin( x * M_PI/180 );;
    }
    */

}
//------------------------------------------------------------------------------------
//! Тут мы создаем объект newPoint – то есть точку, которая должны быть следующей добавлена на график.
//! Соответственно, увеличиваем счетчик counter. И вызываем метод appendGraphPoint().
void Plot::timerHandle()
{
    QPointF newPoint = QPointF(m_x[m_counter],
                               m_y[m_counter]);
    m_counter++;

    appendGraphPoint(newPoint);
}
//------------------------------------------------------------------------------------
//! Добавить точку на отображение
void Plot::appendGraphPoint(QPointF point)
{
    CurveData *data = static_cast<CurveData *>( m_curve->data() );
    data->appendDataPoint(point);
    m_directPainter->drawSeries(m_curve, 0, data->size() - 1);
}
//------------------------------------------------------------------------------------
//!
/*
void Plot::setTestData()
{
    BufferData *bufferData = new BufferData();

    //! Заполним массивы данных, которые мы хотим отобразить:

    int N = 2000;
    double amplitude = 80;

    for (int x = 0; x <= N; ++x)
    {
        double v = amplitude * qFastSin( x * M_PI/180 );

        QPointF s( x, v );

        bufferData->append(s);
    }

    CurveData *data = new CurveData();

    data->setSourceData(bufferData);

    m_curve->setData((QwtSeriesData<QPointF> *) data);
}
*/
//------------------------------------------------------------------------------------
//! Настройка диаграммы
void Plot::setupCurve()
{
    //! Создадим кривые, потребуем, чтобы они были сглаженными (RenderAntialiased),
    //! укажем «перышки» (setPen()), и привяжем кривые к соответствующим областям рисования (attach()).
    m_curve = new QwtPlotCurve();

    //! Включить сглаживание
    m_curve->setRenderHint(QwtPlotItem::RenderAntialiased, true );
    m_curve->setPaintAttribute( QwtPlotCurve::ClipPolygons, false );

    //! Рисовать график линиями
    m_curve->setStyle( QwtPlotCurve::Lines );

    //! Рисовать зеленым
    m_curve->setPen( QPen( Qt::green ) );

    //! Задание хранилища данных кривой
    m_curve->setData( new CurveData() );

    //! Привязка кривой к графику
    m_curve->attach(this);
}
//------------------------------------------------------------------------------------
//!
void Plot::setupCanvas()
{
    m_canvas = new QwtPlotCanvas (this);

    // The backing store is important, when working with widget
    // overlays ( f.e rubberbands for zooming ).
    // Here we don't have them and the internal
    // backing store of QWidget is good enough.

    //! Влияет на отрисовку сетки
//    m_canvas->setPaintAttribute( QwtPlotCanvas::BackingStore, false );
//    m_canvas->setBorderRadius( 10 );

    if ( QwtPainter::isX11GraphicsSystem() )
    {
#if QT_VERSION < 0x050000
        // Even if not liked by the Qt development, Qt::WA_PaintOutsidePaintEvent
        // works on X11. This has a nice effect on the performance.

        m_canvas->setAttribute( Qt::WA_PaintOutsidePaintEvent, true );
#endif

        // Disabling the backing store of Qt improves the performance
        // for the direct painter even more, but the canvas becomes
        // a native window of the window system, receiving paint events
        // for resize and expose operations. Those might be expensive
        // when there are many points and the backing store of
        // the canvas is disabled. So in this application
        // we better don't both backing stores.

        if ( m_canvas->testPaintAttribute( QwtPlotCanvas::BackingStore ) )
        {
            m_canvas->setAttribute( Qt::WA_PaintOnScreen, true );
            m_canvas->setAttribute( Qt::WA_NoSystemBackground, true );
        }
    }

    //! Настройка палитры подложки
    m_canvas->setPalette( setupPalette() );

    //! Графику подложку
    setCanvas( m_canvas );
}
//------------------------------------------------------------------------------------
//!
QPalette Plot::setupPalette()
{
    QPalette pal = palette();

#if QT_VERSION >= 0x040400
    QLinearGradient gradient;
    gradient.setCoordinateMode( QGradient::StretchToDeviceMode );
    gradient.setColorAt( 0.0, QColor( 0, 49, 110 ) );
    gradient.setColorAt( 1.0, QColor( 0, 87, 174 ) );

    pal.setBrush( QPalette::Window, QBrush( gradient ) );
#else
    pal.setBrush( QPalette::Window, QBrush( color ) );
#endif

    // QPalette::WindowText is used for the curve color
    pal.setColor( QPalette::WindowText, Qt::green );

    return pal;
}
//------------------------------------------------------------------------------------
//!
void Plot::setupGrid()
{
    m_grid = new QwtPlotGrid();
    m_grid->setPen( Qt::gray, 0.0, Qt::DotLine );
    m_grid->enableX( true );
    m_grid->enableXMin( true );
    m_grid->enableY( true );
    m_grid->enableYMin( false );
    m_grid->attach( this );
}
//------------------------------------------------------------------------------------
//!
/*
void Plot::replot()
{
    //! Получить хранилище данных кривой
    CurveData *data = ( CurveData * )m_curve->data();

    //! Заблокировать хранилище
    data->values()->lock();

    //! Перерисовать диаграмму
    QwtPlot::replot();

    //! Выяснить колличество точек в кривой
    m_paintedPoints = data->values()->size();

    //! Разблокировать хранилище
    data->values()->unlock();

}
//------------------------------------------------------------------------------------
//!
void Plot::showEvent( QShowEvent * )
{
    replot();
}
//------------------------------------------------------------------------------------
//!
void Plot::timerEvent( QTimerEvent *event )
{
    if ( event->timerId() == m_timerId )
    {
        //updateCurve();

        const double elapsed = m_clock.elapsed() / 1000.0;

        //if ( elapsed > m_interval.maxValue() )
        //    incrementInterval();
        //return;

    }

    replot();

    QwtPlot::timerEvent( event );
}
//------------------------------------------------------------------------------------
//!
void Plot::resizeEvent( QResizeEvent *event )
{
    //d_directPainter->reset();
    QwtPlot::resizeEvent( event );
}
//------------------------------------------------------------------------------------
//!
CurveData * Plot::curve()
{
    return (CurveData *) m_curve->data();
}
//------------------------------------------------------------------------------------
//! Обновить кривую
void Plot::updateCurve()
{
    //! Получить хранилище данных кривой
    CurveData *data = ( CurveData * )m_curve->data();

    //! Заблокировать хранилище данных кривой
    data->values()->lock();

    //! Выяснить колличество точек в кривой
    const int numPoints = data->values()->size();

    //! Если колличество точек для отрисовки стало больше с прошлого момента отрисовки
    //! Появились новые точки, которые еще не нарисованы
    if ( numPoints > m_paintedPoints )
    {
        //! Определить свойство подложки, влияющее на скорость отрисовки
        const bool doClip = !canvas()->testAttribute( Qt::WA_PaintOnScreen );

        if ( doClip )
        {

                //Depending on the platform setting a clip might be an important
                //performance issue. F.e. for Qt Embedded this reduces the
                //part of the backing store that has to be copied out - maybe
                //to an unaccelerated frame buffer device.


            const QwtScaleMap xMap = canvasMap( m_curve->xAxis() );
            const QwtScaleMap yMap = canvasMap( m_curve->yAxis() );

            QRectF br = qwtBoundingRect( *data, m_paintedPoints - 1, numPoints - 1 );

            const QRect clipRect = QwtScaleMap::transform( xMap, yMap, br ).toRect();
            m_directPainter->setClipRegion( clipRect );
        }

        m_directPainter->drawSeries(m_curve, m_paintedPoints - 1, numPoints - 1 );

        //! Обновить колличество нарисованых точек
        m_paintedPoints = numPoints;
    }

    //! Разблокировать хранилище данных кривой
    data->values()->unlock();
}
*/
