#ifndef PLOT_H
#define PLOT_H
//------------------------------------------------------------------------------------
#include <QWidget>
#include <Qwt/qwt_plot.h>
#include <Qwt/qwt_plot_curve.h>
#include <Qwt/qwt_plot_grid.h>
#include <Qwt/qwt_plot_canvas.h>
#include <Qwt/qwt_painter.h>
#include <Qwt/qwt_plot_directpainter.h>
#include <Qwt/qwt_system_clock.h>
#include <QTimerEvent>
#include <QShowEvent>
//#include <>
//#include <>
//#include <>
//#include <>

//#include ""
//#include ""
//#include ""
#include "TimeScaleDraw.h"
#include "BufferData.h"
#include "CurveData.h"
//------------------------------------------------------------------------------------
//!
class Plot : public QwtPlot
{
        Q_OBJECT
    public:
        explicit Plot(QWidget *parent = 0);

        ~Plot();

        CurveData* curve();

    signals:

    public slots:

        void timerHandle();
        //void replot();

    protected:
        //virtual void showEvent( QShowEvent * );
        //virtual void resizeEvent( QResizeEvent * );
        //virtual void timerEvent( QTimerEvent * );

    private:
        //! Настройка диаграммы
        void setupCurve();

        //! Настройка подложки
        void setupCanvas();

        //! Настройка палитры подложки
        QPalette setupPalette();

        //! Настройка сетки
        void setupGrid();

        void setTestData();

        //! Обновить кривую
        void updateCurve();


    private:
        //! Таймер для частоты обновления
        QTimer          *m_updateDataTimer;

        //! Диаграмма
        QwtPlotCurve    *m_curve;

        //! Сетка
        QwtPlotGrid     *m_grid;

        //! Подложка
        QwtPlotCanvas   *m_canvas;

        //! Часы Qwt для более точного времени
        QwtSystemClock  m_clock;

        //! Отрисовка подмножества, для ускорения
        QwtPlotDirectPainter *m_directPainter;

        //! Интервал в секундах ( = 10)
        double          m_interval;

        int             m_timerId;

        //! Колличество отрисованых точек (на начало отрисовки = 0)
        int             m_paintedPoints;

        //QwtInterval     d_interval;

    public slots:
        //void timerHandle();

    private:

        //QwtPlotCurve *curve;

        //QwtPlotDirectPainter *painter;

        void setData();

        void appendGraphPoint(QPointF point);

        int m_counter;

        double m_x[1000];

        double m_y[1000];

};
//------------------------------------------------------------------------------------
#endif // PLOT_H
