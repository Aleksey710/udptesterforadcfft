#include "StreamReportWidget.h"
//------------------------------------------------------------------------------------
//!
StreamReportWidget::StreamReportWidget(QWidget *parent)
                   :QSplitter(Qt::Vertical, parent),
                   //:QGroupBox(, parent),
                    m_layout ( new QVBoxLayout() ),
                    m_buttons ( new QWidget(this) ),
                    m_buttonLayout ( new QHBoxLayout() )
{
    setLayout(m_layout);

    setStyleSheet(//"font: 12px; "
                  //"font-weight: bold; "
                  "margin: 0px, 0px, 0px, 0px;"
                  "padding: 0px, 0px, 0px, 0px; "
                  //"border:  3px solid black; "
                  );

    m_previos = new QPushButton("<<", this);
    m_previos->setStyleSheet(//"font: 12px; "
                             //"font-weight: bold; "
                             "margin: 0px, 0px, 0px, 0px;"
                             "padding: 0px, 0px, 0px, 0px; "
                             //"border:  1px solid black; "
                             );
    m_buttonLayout->addWidget(m_previos);
    connect(m_previos, SIGNAL(clicked()), this, SLOT(previosFrame()));

    m_next = new QPushButton(">>", this);
    m_next->setStyleSheet(//"font: 12px; "
                          //"font-weight: bold; "
                          "margin: 0px, 0px, 0px, 0px;"
                          "padding: 0px, 0px, 0px, 0px; "
                          //"border:  1px solid black; "
                          );

    m_buttonLayout->addWidget(m_next);

    connect(m_next, SIGNAL(clicked()), this, SLOT(nextFrame()));

    m_buttons->setLayout(m_buttonLayout);

    m_layout->addWidget(m_buttons);
}
//------------------------------------------------------------------------------------
//!
StreamReportWidget::~StreamReportWidget()
{
    //qDeleteAll(m_listPlotWidget);
    delete m_buttonLayout;
    delete m_layout;
}
//------------------------------------------------------------------------------------
//!
void StreamReportWidget::addPlotWidget(const int &id)
{
    PlotWidget *plotWidget = new PlotWidget();

    plotWidget->setStyleSheet(//"font: 12px; "
                              //"font-weight: bold; "
                              "margin: 0px, 0px, 0px, 0px;"
                              "padding: 0px, 0px, 0px, 0px; "
                              //"border:  1px solid black; "
                              );

    connect(plotWidget, SIGNAL( curentFromDateTimeChanged(const qint64 &) ),
            this, SIGNAL ( curentFromDateTimeChanged(const qint64 &) ));

    m_layout->addWidget(plotWidget);

    m_listPlotWidget.insert(id, plotWidget);
}
//------------------------------------------------------------------------------------
//!
void StreamReportWidget::previosFrame()
{
    //qDebug() << "StreamReportWidget::previosFrame()";

    for (int i = 0; i < m_listPlotWidget.size(); ++i)
    {
        m_listPlotWidget[i]->previosFrame();
    }

}
//------------------------------------------------------------------------------------
//!
void StreamReportWidget::nextFrame()
{
    //qDebug() << "StreamReportWidget::nextFrame()";

    for (int i = 0; i < m_listPlotWidget.size(); ++i)
    {
        m_listPlotWidget[i]->nextFrame();
    }
}
//------------------------------------------------------------------------------------
//!
