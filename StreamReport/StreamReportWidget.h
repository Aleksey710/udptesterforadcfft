#ifndef STREAMREPORTWIDGET_H
#define STREAMREPORTWIDGET_H
//------------------------------------------------------------------------------------
#include <QWidget>
#include <QSplitter>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
//#include <>
#include <qwt_plot_renderer.h>
#include "PlotRenderer.h"
//#include <>
//#include <>
//#include <>

//#include ""
//#include ""

#include "CurveData.h"
#include "PlotView.h"
#include "PlotWidget.h"
#include "ScrollZoomer.h"
#include "Zoomer.h"


//------------------------------------------------------------------------------------
//!
class StreamReportWidget : public QSplitter
//class StreamReportWidget : public QGroupBox
{
        Q_OBJECT
    public:
        explicit StreamReportWidget(QWidget *parent = 0);
        virtual ~StreamReportWidget();

        void addPlotWidget(const int &id);

        inline PlotWidget* plotWidgetAt(int id)
            { return m_listPlotWidget.value(id, NULL); }


    signals:
        void curentFromDateTimeChanged(const qint64 &);

    public slots:

    private slots:
        void previosFrame();
        void nextFrame();

    private:
        QVBoxLayout                 *m_layout;

        QWidget                     *m_buttons;

        QHBoxLayout                 *m_buttonLayout;

        QPushButton                 *m_previos;
        QPushButton                 *m_next;

        //! Список отображений графиков
        QMap<int, PlotWidget*>      m_listPlotWidget;

};
//------------------------------------------------------------------------------------
#endif // STREAMREPORTWIDGET_H
