################################################################
# Qwt Widget Library
# Copyright (C) 1997   Josef Wilgen
# Copyright (C) 2002   Uwe Rathmann
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the Qwt License, Version 1.0
################################################################

#CONFIG   += ordered

################################################################

QWT_VER_MAJ      = 6
QWT_VER_MIN      = 1
QWT_VER_PAT      = 0
QWT_VERSION      = $${QWT_VER_MAJ}.$${QWT_VER_MIN}.$${QWT_VER_PAT}

######################################################################
# Install paths
######################################################################

#QWT_INSTALL_PREFIX = $$[QT_INSTALL_PREFIX]

#unix {
#    QWT_INSTALL_PREFIX    = /usr/local/qwt-$$QWT_VERSION
#}

#win32 {
#    QWT_INSTALL_PREFIX    = C:/Qwt-$$QWT_VERSION
#}

#QWT_INSTALL_DOCS      = $${QWT_INSTALL_PREFIX}/doc
#QWT_INSTALL_HEADERS   = $${QWT_INSTALL_PREFIX}/include
#QWT_INSTALL_LIBS      = $${QWT_INSTALL_PREFIX}/lib

######################################################################
# Designer plugin
# creator/designer load designer plugins from certain default
# directories ( f.e the path below QT_INSTALL_PREFIX ) and the
# directories listed in the QT_PLUGIN_PATH environment variable.
# When using the path below QWT_INSTALL_PREFIX you need to
# add $${QWT_INSTALL_PREFIX}/plugins to QT_PLUGIN_PATH in the
# runtime environment of designer/creator.
######################################################################

#QWT_INSTALL_PLUGINS   = $${QWT_INSTALL_PREFIX}/plugins/designer

# linux distributors often organize the Qt installation
# their way and QT_INSTALL_PREFIX doesn't offer a good
# path. Also QT_INSTALL_PREFIX is only one of the default
# search paths of the designer - not the Qt creator

#QWT_INSTALL_PLUGINS   = $$[QT_INSTALL_PREFIX]/plugins/designer

######################################################################
# Features
# When building a Qwt application with qmake you might want to load
# the compiler/linker flags, that are required to build a Qwt application
# from qwt.prf. Therefore all you need to do is to add "CONFIG += qwt"
# to your project file and take care, that qwt.prf can be found by qmake.
# ( see http://doc.trolltech.com/4.7/qmake-advanced-usage.html#adding-new-configuration-features )
# I recommend not to install the Qwt features together with the
# Qt features, because you will have to reinstall the Qwt features,
# with every Qt upgrade.
######################################################################

#QWT_INSTALL_FEATURES  = $${QWT_INSTALL_PREFIX}/features
# QWT_INSTALL_FEATURES  = $$[QT_INSTALL_PREFIX]/features

######################################################################
# Build the static/shared libraries.
# If QwtDll is enabled, a shared library is built, otherwise
# it will be a static library.
######################################################################

#QWT_CONFIG           += QwtDll

######################################################################
# QwtPlot enables all classes, that are needed to use the QwtPlot
# widget.
######################################################################

QWT_CONFIG       += QwtPlot

######################################################################
# QwtWidgets enables all classes, that are needed to use the all other
# widgets (sliders, dials, ...), beside QwtPlot.
######################################################################

QWT_CONFIG     += QwtWidgets

######################################################################
# If you want to display svg images on the plot canvas, or
# export a plot to a SVG document
######################################################################

QWT_CONFIG     += QwtSvg

######################################################################
# If you want to use a OpenGL plot canvas
######################################################################

#QWT_CONFIG     += QwtOpenGL

######################################################################
# You can use the MathML renderer of the Qt solutions package to
# enable MathML support in Qwt. Because of license implications
# the ( modified ) code of the MML Widget solution is included and
# linked together with the QwtMathMLTextEngine into an own library.
# To use it you will have to add "CONFIG += qwtmathml"
# to your qmake project file.
######################################################################

#QWT_CONFIG     += QwtMathML

######################################################################
# If you want to build the Qwt designer plugin,
# enable the line below.
# Otherwise you have to build it from the designer directory.
######################################################################

#QWT_CONFIG     += QwtDesigner

######################################################################
# Compile all Qwt classes into the designer plugin instead
# of linking it against the shared Qwt library. Has no effect
# when QwtDesigner or QwtDll are not both enabled.
#
# On systems where rpath is supported ( all Unixoids ) the
# location of the installed Qwt library is compiled into the plugin,
# but on Windows it might be easier to have a self contained
# plugin to avoid any hassle with configuring the runtime
# environment of the designer/creator.
######################################################################

#win32 {
#    QWT_CONFIG     += QwtDesignerSelfContained
#}

######################################################################
# If you want to auto build the examples, enable the line below
# Otherwise you have to build them from the examples directory.
######################################################################

#QWT_CONFIG     += QwtExamples

######################################################################
# The playground is primarily intended for the Qwt development
# to explore and test new features. Nevertheless you might find
# ideas or code snippets that help for application development
# If you want to auto build the applications in playground, enable
# the line below.
# Otherwise you have to build them from the playground directory.
######################################################################

#QWT_CONFIG     += QwtPlayground

######################################################################
# When Qt has been built as framework qmake wants
# to link frameworks instead of regular libs
######################################################################

#macx:!static:CONFIG(qt_framework, qt_framework|qt_no_framework) {
#    QWT_CONFIG += QwtFramework
#}

######################################################################
# qmake internal options
######################################################################

CONFIG           += qt
CONFIG           += warn_on
CONFIG           += no_keywords
CONFIG           += silent

#############################################################
#include(../../TDM.pri)

######################################################################
# release/debug mode
######################################################################

win32 {
    # On Windows you can't mix release and debug libraries.
    # The designer is built in release mode. If you like to use it
    # you need a release version. For your own application development you
    # might need a debug version.
    # Enable debug_and_release + build_all if you want to build both.

#    CONFIG           += debug_and_release
#    CONFIG           += build_all
}
else {

#    CONFIG           += release

#    VER_MAJ           = $${QWT_VER_MAJ}
#    VER_MIN           = $${QWT_VER_MIN}
#    VER_PAT           = $${QWT_VER_PAT}
#    VERSION           = $${QWT_VERSION}
}

linux-g++ | linux-g++-64 {
    #CONFIG           += separate_debug_info
    #QMAKE_CXXFLAGS   *= -Wfloat-equal
    #QMAKE_CXXFLAGS   *= -Wshadow
    #QMAKE_CXXFLAGS   *= -Wpointer-arith
    #QMAKE_CXXFLAGS   *= -Wconversion
    #QMAKE_CXXFLAGS   *= -Wsign-compare
    #QMAKE_CXXFLAGS   *= -Wsign-conversion
    #QMAKE_CXXFLAGS   *= -Wlogical-op
    #QMAKE_CXXFLAGS   *= -Werror=format-security
    #QMAKE_CXXFLAGS   *= -std=c++11

    # when using the gold linker ( Qt < 4.8 ) - might be
    # necessary on non linux systems too
    #QMAKE_LFLAGS += -lrt
}

######################################################################
# paths for building qwt
######################################################################

TARGET            = Qwt

#############################################################
# Для библиотеки
TEMPLATE     = lib
CONFIG      += staticlib
DEPENDPATH  += ../lib/
DESTDIR      = ../lib/

#CONFIG     += dll

#############################################################
OBJECTS_DIR = ../build/Qwt
MOC_DIR     = ../build/Qwt
UI_DIR      = ../build/Qwt
RCC_DIR     = ../build/Qwt

################################################################
INCLUDEPATH += ./


################################################################
HEADERS += \
    qwt.h \
    qwt_abstract_scale_draw.h \
    qwt_clipper.h \
    qwt_color_map.h \
    qwt_compat.h \
    qwt_column_symbol.h \
    qwt_date.h \
    qwt_date_scale_draw.h \
    qwt_date_scale_engine.h \
    qwt_dyngrid_layout.h \
    qwt_global.h \
    qwt_graphic.h \
    qwt_interval.h \
    qwt_interval_symbol.h \
    qwt_math.h \
    qwt_magnifier.h \
    qwt_null_paintdevice.h \
    qwt_painter.h \
    qwt_painter_command.h \
    qwt_panner.h \
    qwt_picker.h \
    qwt_picker_machine.h \
    qwt_pixel_matrix.h \
    qwt_point_3d.h \
    qwt_point_polar.h \
    qwt_round_scale_draw.h \
    qwt_scale_div.h \
    qwt_scale_draw.h \
    qwt_scale_engine.h \
    qwt_scale_map.h \
    qwt_spline.h \
    qwt_symbol.h \
    qwt_system_clock.h \
    qwt_text_engine.h \
    qwt_text_label.h \
    qwt_text.h \
    qwt_transform.h \
    qwt_widget_overlay.h

SOURCES += \
    qwt_abstract_scale_draw.cpp \
    qwt_clipper.cpp \
    qwt_color_map.cpp \
    qwt_column_symbol.cpp \
    qwt_date.cpp \
    qwt_date_scale_draw.cpp \
    qwt_date_scale_engine.cpp \
    qwt_dyngrid_layout.cpp \
    qwt_event_pattern.cpp \
    qwt_graphic.cpp \
    qwt_interval.cpp \
    qwt_interval_symbol.cpp \
    qwt_math.cpp \
    qwt_magnifier.cpp \
    qwt_null_paintdevice.cpp \
    qwt_painter.cpp \
    qwt_painter_command.cpp \
    qwt_panner.cpp \
    qwt_picker.cpp \
    qwt_picker_machine.cpp \
    qwt_pixel_matrix.cpp \
    qwt_point_3d.cpp \
    qwt_point_polar.cpp \
    qwt_round_scale_draw.cpp \
    qwt_scale_div.cpp \
    qwt_scale_draw.cpp \
    qwt_scale_map.cpp \
    qwt_spline.cpp \
    qwt_scale_engine.cpp \
    qwt_symbol.cpp \
    qwt_system_clock.cpp \
    qwt_text_engine.cpp \
    qwt_text_label.cpp \
    qwt_text.cpp \
    qwt_transform.cpp \
    qwt_widget_overlay.cpp


contains(QWT_CONFIG, QwtPlot) {

    HEADERS += \
        qwt_curve_fitter.h \
        qwt_event_pattern.h \
        qwt_abstract_legend.h \
        qwt_legend.h \
        qwt_legend_data.h \
        qwt_legend_label.h \
        qwt_plot.h \
        qwt_plot_renderer.h \
        qwt_plot_curve.h \
        qwt_plot_dict.h \
        qwt_plot_directpainter.h \
        qwt_plot_grid.h \
        qwt_plot_histogram.h \
        qwt_plot_item.h \
        qwt_plot_abstract_barchart.h \
        qwt_plot_barchart.h \
        qwt_plot_multi_barchart.h \
        qwt_plot_intervalcurve.h \
        qwt_plot_tradingcurve.h \
        qwt_plot_layout.h \
        qwt_plot_marker.h \
        qwt_plot_zoneitem.h \
        qwt_plot_textlabel.h \
        qwt_plot_rasteritem.h \
        qwt_plot_spectrogram.h \
        qwt_plot_spectrocurve.h \
        qwt_plot_scaleitem.h \
        qwt_plot_legenditem.h \
        qwt_plot_seriesitem.h \
        qwt_plot_shapeitem.h \
        qwt_plot_canvas.h \
        qwt_plot_panner.h \
        qwt_plot_picker.h \
        qwt_plot_zoomer.h \
        qwt_plot_magnifier.h \
        qwt_plot_rescaler.h \
        qwt_point_mapper.h \
        qwt_raster_data.h \
        qwt_matrix_raster_data.h \
        qwt_sampling_thread.h \
        qwt_samples.h \
        qwt_series_data.h \
        qwt_series_store.h \
        qwt_point_data.h \
        qwt_scale_widget.h

    SOURCES += \
        qwt_curve_fitter.cpp \
        qwt_abstract_legend.cpp \
        qwt_legend.cpp \
        qwt_legend_data.cpp \
        qwt_legend_label.cpp \
        qwt_plot.cpp \
        qwt_plot_renderer.cpp \
        qwt_plot_xml.cpp \
        qwt_plot_axis.cpp \
        qwt_plot_curve.cpp \
        qwt_plot_dict.cpp \
        qwt_plot_directpainter.cpp \
        qwt_plot_grid.cpp \
        qwt_plot_histogram.cpp \
        qwt_plot_item.cpp \
        qwt_plot_abstract_barchart.cpp \
        qwt_plot_barchart.cpp \
        qwt_plot_multi_barchart.cpp \
        qwt_plot_intervalcurve.cpp \
        qwt_plot_zoneitem.cpp \
        qwt_plot_tradingcurve.cpp \
        qwt_plot_spectrogram.cpp \
        qwt_plot_spectrocurve.cpp \
        qwt_plot_scaleitem.cpp \
        qwt_plot_legenditem.cpp \
        qwt_plot_seriesitem.cpp \
        qwt_plot_shapeitem.cpp \
        qwt_plot_marker.cpp \
        qwt_plot_textlabel.cpp \
        qwt_plot_layout.cpp \
        qwt_plot_canvas.cpp \
        qwt_plot_panner.cpp \
        qwt_plot_rasteritem.cpp \
        qwt_plot_picker.cpp \
        qwt_plot_zoomer.cpp \
        qwt_plot_magnifier.cpp \
        qwt_plot_rescaler.cpp \
        qwt_point_mapper.cpp \
        qwt_raster_data.cpp \
        qwt_matrix_raster_data.cpp \
        qwt_sampling_thread.cpp \
        qwt_series_data.cpp \
        qwt_point_data.cpp \
        qwt_scale_widget.cpp
}

greaterThan(QT_MAJOR_VERSION, 4) {

    QT += printsupport
    QT += concurrent
}

contains(QWT_CONFIG, QwtSvg) {

    QT += svg

    HEADERS += qwt_plot_svgitem.h
    SOURCES += qwt_plot_svgitem.cpp
}
else {

    DEFINES += QWT_NO_SVG
}

contains(QWT_CONFIG, QwtOpenGL) {

    QT += opengl

    HEADERS += qwt_plot_glcanvas.h
    SOURCES += qwt_plot_glcanvas.cpp
}
else {

    DEFINES += QWT_NO_OPENGL
}

contains(QWT_CONFIG, QwtWidgets) {

    HEADERS += \
        qwt_abstract_slider.h \
        qwt_abstract_scale.h \
        qwt_arrow_button.h \
        qwt_analog_clock.h \
        qwt_compass.h \
        qwt_compass_rose.h \
        qwt_counter.h \
        qwt_dial.h \
        qwt_dial_needle.h \
        qwt_knob.h \
        qwt_slider.h \
        qwt_thermo.h \
        qwt_wheel.h

    SOURCES += \
        qwt_abstract_slider.cpp \
        qwt_abstract_scale.cpp \
        qwt_arrow_button.cpp \
        qwt_analog_clock.cpp \
        qwt_compass.cpp \
        qwt_compass_rose.cpp \
        qwt_counter.cpp \
        qwt_dial.cpp \
        qwt_dial_needle.cpp \
        qwt_knob.cpp \
        qwt_slider.cpp \
        qwt_thermo.cpp \
        qwt_wheel.cpp
}
#############################################################
